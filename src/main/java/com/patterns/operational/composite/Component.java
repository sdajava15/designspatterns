package com.patterns.operational.composite;

public abstract class Component {
    public int value;

    public Component(int value) {
        this.value = value;
    }

    public abstract void printValue() ;


}
