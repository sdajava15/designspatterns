package com.patterns.operational.composite;

public class Leaf extends Component {
    public Leaf(int value) {
        super(value);
    }

    @Override
    public void printValue() {
        System.out.println(this.value);
    }


}
