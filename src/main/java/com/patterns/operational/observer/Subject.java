package com.patterns.operational.observer;

import java.util.ArrayList;
import java.util.List;

public abstract class Subject {
    private List<Observer> observers;
    protected String name;

    protected Subject(String name) {
        observers = new ArrayList<>();
        this.name = name;
    }


    public void attach(Observer observer) {
        observers.add(observer);
    }

    public void detach(Observer observer) {
        observers.remove(observer);
    }

    public void promote() {
        for (Observer observer: observers) {
            observer.update(name);
        }
    }

}
