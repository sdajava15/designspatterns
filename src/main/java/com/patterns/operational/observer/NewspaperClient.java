package com.patterns.operational.observer;

public class NewspaperClient implements Observer{

    private final String name;

    public NewspaperClient(String name) {
        this.name = name;
    }


    @Override
    public void update(String name) {
        System.out.println( this.name + " przeczytał/a gazetę " + name + ".");
    }



}
