package com.patterns.operational.adapter;

public class WizzardAdapter implements Person {
    private Wizzard wizzard;


    public WizzardAdapter() {
        this.wizzard = new Wizzard();
    }


    public void attack() {
        this.wizzard.castDestructionSpell();
    }

    public void defend() {
        this.wizzard.defend();
    }


    public void run() {
        this.wizzard.run();
    }

}
