package com.patterns.operational.adapter;

public interface Person {
    void attack();
    void defend();
    void run();
}
