package com.patterns.operational.adapter;

public class WarriorAdapter implements Person {

    private Warrior warrior;

    public WarriorAdapter() {
        this.warrior = new Warrior();
    }

    public void attack() {
        this.warrior.swingSword();
    }

    public void defend() {
        this.warrior.defendUsingShield();
    }


    public void run() {
        this.warrior.run();
    }

}
