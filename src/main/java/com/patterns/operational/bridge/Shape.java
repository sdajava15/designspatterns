package com.patterns.operational.bridge;

public abstract class Shape {

    private final Drawing drawing;

    protected Shape(Drawing drawing) {
        this.drawing = drawing;
    }


    public abstract void draw();


    protected void drawLine() {
        this.drawing.drawLine();
    }

    protected void drawCircle() {
        this.drawing.drawCircle();
    }

}
