package com.patterns.operational.bridge;

public class Rectangle extends Shape {

    public Rectangle(Drawing drawing) {
        super(drawing);
    }

    @Override
    public void draw() {
        this.drawLine();
        this.drawLine();
        this.drawLine();
        this.drawLine();
    }
}
