package com.patterns.operational.bridge;

public class Photoshop extends Drawing {

    private PhotoshopApi photoshopApi;


    public Photoshop() {
        this.photoshopApi = new PhotoshopApi();

    }

    @Override
    public void drawLine() {
        photoshopApi.drawALine();
    }

    @Override
    public void drawCircle() {
        photoshopApi.drawACircle();
    }
}
