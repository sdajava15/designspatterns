package com.patterns.operational.carDecorator;

public final class Bmw extends CarDecorator {

    private Bmw(Car car) {
        super(car);
    }

    public Bmw() {
        this(new AverageCar());
    }


    @Override
    public void drive() {
        super.drive();
        System.out.println("Jestem właścicielem bmw więc palę gumę.");
    }


    public void makeSoup() {
        System.out.println("Ktoś wrzucił grant do środka - mamy barszyczk.");
    }



}
