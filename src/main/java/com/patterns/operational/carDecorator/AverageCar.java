package com.patterns.operational.carDecorator;

public final class AverageCar implements Car {
    @Override
    public void enter() {
        System.out.println("Otwierasz drzwi klamką.");
    }

    @Override
    public void drive() {
        System.out.println("Uruchamiasz silnik i ruszasz z kopyta.");
    }

    @Override
    public void stop() {
        System.out.println("Szukasz wolnego miejsca i parkujesz.");
    }
}
