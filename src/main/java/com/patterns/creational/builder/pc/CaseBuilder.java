package com.patterns.creational.builder.pc;

import com.patterns.creational.builder.Builder;

public class CaseBuilder implements Builder<PersonalComputer>,
        RamBuilder,
        DriveBuilder,
        ProcessorBuilder,
        MainboardBuilder,
        GraphicsCardBuilder {


    private PersonalComputer product;

    // GraphicsCard -> Mainboard -> Processor -> Drive -> RAM -> Case ->

    public PersonalComputer build() {
        return product;
    }


    public CaseBuilder() {
        product = new PersonalComputer();
    }


    public RamBuilder setCase(Case pcCase) {
        product.setPcCase(pcCase);
        return this;
    }

    public DriveBuilder setRam(RAM ram) {
        product.setRam(ram);
        return this;
    }


    public ProcessorBuilder setDrive(Drive drive) {
        product.setDrive(drive);
        return this;
    }

    public MainboardBuilder setProcessor(Processor processor) {
        product.setProcessor(processor);
        return this;
    }

    public GraphicsCardBuilder setMainboard(Mainboard mainboard) {
        product.setMainboard(mainboard);
        return this;
    }


    public CaseBuilder setGraphicalCard(GraphicsCard card) {
        this.product.setGraphicsCard(card);
        return this;
    }


}
