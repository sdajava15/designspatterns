package com.patterns.creational.builder.pc;

public interface DriveBuilder {
    ProcessorBuilder setDrive(Drive drive);
}

// GraphicsCard -> Mainboard -> Processor -> Drive -> RAM -> Case ->