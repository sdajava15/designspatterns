package com.patterns.creational.builder.pc;

public interface RamBuilder {
    DriveBuilder setRam(RAM ram);
}

// GraphicsCard -> Mainboard -> Processor -> Drive -> RAM -> Case ->
