package com.patterns.creational.builder.pc;

public enum Case {
    MiniTower,
    Tower,
    MidiTower,
    HighTower,
    Desktop

}
