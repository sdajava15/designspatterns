package com.patterns.creational.builder.pc;

public enum RAM {
    Goodram,
    Kingstone,
    ADATA,
    Patriot,
    Corsair

}
